package com.mycompany.promocalculator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * This object represent goods in our shop. 
 * @version 1.0
 * @author dmitry.bulyan
 *
 */
@XmlRootElement(namespace = "com.mycompany.promocalculator.product")
public class Product {	
	protected String name;
	protected String group;
	protected Float price;
	
	/**
	 * This returns name of product  
	 * @return product name
	 */
	@XmlElement(name = "name")
	public String getName() {
		return name;
	}
	/**
	 * Setting name for product
	 * @param name product name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * This returns group of product
	 * @return product group
	 */
	public String getGroup() {
		return group;
	}
	/**
	 * Setting product group
	 * @param group group name for product
	 */	
	public void setGroup(String group) {
		this.group = group;
	}
	
	/**
	 * This returns price of product
	 * @return product price
	 */
	@XmlElement(name = "price")
	public Float getPrice() {
		return price;
	}
	/**
	 * Setting price for product
	 * @param price product price
	 */

	public void setPrice(Float price) {
		this.price = price;
	}
	/**
	 * Override default method
	 */
	@Override
	public String toString() {
		String s = "	" + name + "  " + price;
		if (group != null) {
			s = group + s;
		}
		return s;
	}
}
