package com.mycompany.promocalculator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *  List of goods which have been chosen by customer. 
 */
@XmlRootElement
public class Invoice {
	private HashMap<String, Integer> invoice = new HashMap<String, Integer>();
	private final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
	private String invoiceName = "";
	/**
	 * Returns name of invoice, need to identify this invoice.
	 * @return invoice name
	 */
	@XmlElement(name="invoicename")
	public String getInvoiceName() {
		return invoiceName;
	}
	/**
	 * Set name for invoice, need to identify this invoice.
	 * @param invoiceName invoice name
	 */
	public void setInvoiceName(String invoiceName) {
		this.invoiceName = invoiceName;
	}
	/**
	 * Add product to invoice
	 * @param pName product name
	 * @param quantity quantity of product to add 
	 * @return
	 */
	public Integer addProduct(String pName, Integer quantity) {
		logger.debug("added {} quantity {} ",pName,quantity);
		return invoice.put(pName, quantity);
	}
	/**
	 * Returns quantity of product in invoice. 
	 * @param pName product name
	 * @return quantity
	 */
	public Integer getProductQuantity(String pName) {
		return invoice.get(pName);
	}
	/**
	 * Returns list of product name
	 * @return iterator from list of product name
	 */
	@XmlElement(name="product list")
	public Iterator<String> getProductNames() {
		Set<String> keys = invoice.keySet();
		return keys.iterator();
	}
	/**
	 * Removes product from invoice.
	 * @param name product name
	 */
	public void removeProduct(String name){
		invoice.remove(name);
	}
}
