package com.mycompany.promocalculator.command;

import com.mycompany.promocalculator.Context;

public class CommandFactory {

	private static CommandFactory instance;
	private final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	private CommandFactory() {	
	}

	public static CommandFactory getInstance() {
		if (instance == null) {
			instance = new CommandFactory();
		}
		return instance;
	}

	public Command createCommand(String commandName) {
		Object c = null;
		try {
			if (Context.state != Context.DISCOUNTLIST) {
				c = Class.forName(CommandEnum.getCommandClassName(commandName)).newInstance();
			} else
				throw new IllegalAccessException();

		} catch (InstantiationException | IllegalAccessException | NullPointerException | ClassNotFoundException e) {
			c = new ShowAvailableCommand();
			logger.info("\"{}\" - unknown command ", commandName);
			logger.debug(e.getMessage(), e);
		}
		Command cmd = (Command) c;
		return cmd;
	}
}